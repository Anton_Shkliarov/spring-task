package ua.com.epam.spring.task.aspects;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Aspect
public class CounterAspect {

    private Map<String, Integer> nameCounter = new HashMap<>();

    private Map<Integer, Integer> priceCounter = new HashMap<>();

    @Pointcut("execution(public String getEventName(..))")
    private void anyEventGetName() {}

    @Pointcut("execution(public int getBaseTicketPrice(..))")
    private void anyEventGetPrice() {}

    @AfterReturning(pointcut = "anyEventGetName()", returning = "eventName")
    public void countGetNames(String eventName) {
        if (!nameCounter.containsKey(eventName))
            nameCounter.put(eventName, 0);
        nameCounter.put(eventName, nameCounter.get(eventName) + 1);
    }

    @AfterReturning(pointcut = "anyEventGetPrice()", returning = "ticketPrice")
    public void countGetPrices(Integer ticketPrice) {
        if (!priceCounter.containsKey(ticketPrice))
            priceCounter.put(ticketPrice, 0);
        priceCounter.put(ticketPrice, priceCounter.get(ticketPrice) + 1);
    }

    public Map<String, Integer> getNameCounter() {
        return nameCounter;
    }

    public Map<Integer, Integer> getPriceCounter() {
        return priceCounter;
    }
}
