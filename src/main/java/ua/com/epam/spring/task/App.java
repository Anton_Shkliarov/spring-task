package ua.com.epam.spring.task;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    private AppRunner appRunner;

    public void setAppRunner(AppRunner appRunner) {
        this.appRunner = appRunner;
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
        App app = (App) ctx.getBean("app");
        app.appRunner.run();
        ctx.close();
    }
}
