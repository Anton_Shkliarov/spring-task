package ua.com.epam.spring.task.entities;


import java.util.Date;

public class DiscountStrategy {

    public DiscountStrategy() {
    }

    public double getDiscount(User user, Date date) {
        if (user.getBirthDate().equals(date))
            return getBirthdayDiscount();
        if ((user.getBookedTickets() + 1) % 10 == 0 )
            return getEveryTenDiscount();
        return 1;
    }

    private double getBirthdayDiscount() {
        return 0.05;
    }

    private double getEveryTenDiscount() {
        return 0.5;
    }
}
