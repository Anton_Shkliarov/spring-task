package ua.com.epam.spring.task.db;


public class QueryBuilder {

    private static final String SPACE = " ";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";
    private static final String SEMICOLON = ";";

    private StringBuilder query;

    public QueryBuilder() {
        query = new StringBuilder();
    }

    public QueryBuilder(StringBuilder query) {
        this.query = query;
    }

    public QueryBuilder select(String entity) {
        query.append("SELECT");
        query.append(SPACE);
        query.append(entity);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder from(String table) {
        query.append("FROM");
        query.append(SPACE);
        query.append(table);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder where(String condition) {
        query.append("WHERE");
        query.append(SPACE);
        query.append(condition);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder insertInto(String table) {
        query.append("INSERT").append(SPACE).append("INTO");
        query.append(SPACE);
        query.append(table);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder tableFields(String fields) {
        query.append(SPACE).append(OPEN_BRACKET);
        query.append(fields);
        query.append(CLOSE_BRACKET);
        return this;
    }

    public QueryBuilder values(String values) {
        query.append("VALUES").append(SPACE).append(OPEN_BRACKET);
        query.append(values);
        query.append(CLOSE_BRACKET).append(SPACE);
        return this;
    }

    public QueryBuilder createTableWithFields(String tableName, String fields) {
        query.append("CREATE").append(SPACE).append("TABLE").append(SPACE);
        query.append(tableName);
        query.append(OPEN_BRACKET);
        query.append(fields);
        query.append(CLOSE_BRACKET);
        return this;
    }

    public QueryBuilder update(String table) {
        query.append("UPDATE").append(SPACE);
        query.append(table);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder set(String columns) {
        query.append("SET").append(SPACE);
        query.append(columns);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder deleteFrom(String table) {
        query.append("DELETE").append(SPACE).append("FROM").append(SPACE);
        query.append(table);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder dropDB(String dbName) {
        query.append("DROP").append(SPACE).append("DATABASE").append(SPACE);
        query.append(dbName);
        query.append(SPACE);
        return this;
    }

    public QueryBuilder endOfQuery() {
        query.append(SEMICOLON);
        return this;
    }

    public QueryBuilder clearQuery() {
        if (query.length() > 1)
            query.delete(0, query.length());
        else if (query.length() == 1) {
            query.deleteCharAt(0);
        }
        return this;
    }

    @Override
    public String toString() {
        return query.toString();
    }
}
