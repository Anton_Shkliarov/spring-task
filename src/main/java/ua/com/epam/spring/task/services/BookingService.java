package ua.com.epam.spring.task.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.com.epam.spring.task.dao.AuditoriumsDAO;
import ua.com.epam.spring.task.dao.TicketDAO;
import ua.com.epam.spring.task.entities.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    private TicketDAO ticketDAO;

    @Autowired
    private AuditoriumsDAO auditoriumsDAO;

    @Autowired
    private DiscountService discountService;

    public Double getTicketPrice(Event event, String strDate, User user, String... seats) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter.parse(strDate);
        double price = event.getBaseTicketPrice() * seats.length;
        for (String seat : seats) {
            double simplePrice = price / 3;
            Auditorium auditorium = auditoriumsDAO.getAuditoriumByName(event.getAuditorium());
            if (auditorium.getVipSeats().contains(seat)) {
                price = price - simplePrice;
                simplePrice = simplePrice * 2;
                price = price + simplePrice;
            }
        }
        if (event.getRating() == Rating.HIGH)
            price = price * 1.2;
        double discount = discountService.getDiscount(user, date);
        return price * discount;
    }

    public void bookTicket(User user, Ticket ticket) {
        ticket.setBooked(true);
        ticketDAO.createBookedTicket(ticket.getEvent(), ticket.getSeat(), user.getEmail(), ticket.getPrice(), ticket.getStrDate());
    }

    public List<Ticket> getTicketsForEvent(Event event, String strDate) throws ParseException {
        List<Ticket> tickets = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter.parse(strDate);
        for (Ticket ticket : ticketDAO.getAllTickets()) {
            if (ticket.getEvent().equals(event.getEventName()) && ticket.getDate().equals(date))
                tickets.add(ticket);
        }
        return tickets;
    }
}
