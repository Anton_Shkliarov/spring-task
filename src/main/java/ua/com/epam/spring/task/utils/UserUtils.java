package ua.com.epam.spring.task.utils;


public class UserUtils implements UtilsInterface {

    public static void checkNameIsNotNull(String name) {
        if (EMPTY_STRING.equals(name)) {
            throw new IllegalArgumentException("You can't register a user with empty name");
        }
    }

    public static void checkEmailIsNotNull(String email) {
        if (EMPTY_STRING.equals(email)) {
            throw new IllegalArgumentException("You can't register a user without email");
        }
    }

    public static void checkIdIsNotNull(int id) {
        if (id == 0){
            throw new IllegalArgumentException("Id cannot be null");
        }
    }

    public static void checkBirthDateIsNotNull(String birth) {
        if (EMPTY_STRING.equals(birth)) {
            throw new IllegalArgumentException("You can't register a user without date of birth");
        }
    }
}
