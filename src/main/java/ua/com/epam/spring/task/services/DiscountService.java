package ua.com.epam.spring.task.services;


import org.springframework.stereotype.Service;
import ua.com.epam.spring.task.entities.DiscountStrategy;
import ua.com.epam.spring.task.entities.User;

import java.util.Date;

@Service
public class DiscountService {

    private DiscountStrategy discountStrategy;

    public void setDiscountStrategy(DiscountStrategy discountStrategy) {
        this.discountStrategy = discountStrategy;
    }

    public double getDiscount(User user, Date date) {
        return discountStrategy.getDiscount(user, date);
    }
}
