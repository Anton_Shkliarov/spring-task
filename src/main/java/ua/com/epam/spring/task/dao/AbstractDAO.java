package ua.com.epam.spring.task.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.com.epam.spring.task.db.DBManager;

import javax.annotation.Resource;

public abstract class AbstractDAO {

    @Autowired
    @Resource(name = "dbManager")
    private DBManager dbManager;

    protected JdbcTemplate getJDBCTemplate() {
        return dbManager.getJdbcTemplate();
    }

}
