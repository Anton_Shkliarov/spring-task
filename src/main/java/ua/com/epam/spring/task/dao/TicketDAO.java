package ua.com.epam.spring.task.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import ua.com.epam.spring.task.entities.Ticket;
import ua.com.epam.spring.task.db.DBConstants;
import ua.com.epam.spring.task.db.QueryBuilder;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TicketDAO extends AbstractDAO {

    @Autowired
    @Resource(name = "queryBuilder")
    private QueryBuilder query;

    public void createBookedTicket(String event, Integer seat, String user, double price, String date) {
        query.insertInto(DBConstants.TICKETS_TABLE_NAME).values("?, ?, ?, ?, ?, ?");
        getJDBCTemplate().update(query.toString(), event, seat, user, price, date, true);
    }

    public List<Ticket> getAllTickets() {
        query.clearQuery().select("*").from(DBConstants.TICKETS_TABLE_NAME).endOfQuery();
        return getJDBCTemplate().query(query.toString(), getTicketRowMapper());
    }

    public List<Ticket> getTicketsBookedByUser(String userEmail) {
        query.clearQuery().select("*").from(DBConstants.TICKETS_TABLE_NAME).where("user_email = '" + userEmail + "'").endOfQuery();
        return getJDBCTemplate().query(query.toString(), getTicketRowMapper());
    }

    private RowMapper<Ticket> getTicketRowMapper() {
        return new RowMapper<Ticket>() {
            @Override
            public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
                String event = resultSet.getString("event");
                Integer seat = resultSet.getInt("seat_num");
                String user = resultSet.getString("user_name");
                Double price = resultSet.getDouble("price");
                String date = resultSet.getString("date");
                Boolean isBooked = resultSet.getBoolean("is_booked");
                Ticket ticket = new Ticket(event, seat, user, price, date);
                ticket.setBooked(isBooked);
                return ticket;
            }
        };
    }

}
