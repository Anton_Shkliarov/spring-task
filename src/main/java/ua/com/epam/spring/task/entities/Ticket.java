package ua.com.epam.spring.task.entities;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ticket {

    private String event;
    private boolean isBooked;
    private String userEmail;
    private Integer seat;
    private double price;
    private Date date;

    public Ticket(String event, Integer seat, String userEmail, double price, String date) {
        this.event = event;
        this.seat = seat;
        this.userEmail = userEmail;
        this.price = price;
        this.date = parseDate(date);
        isBooked = false;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public String getStrDate() {
        return date.toString();
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date parseDate(String strDate) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "event=" + event +
                ", isBooked=" + isBooked +
                ", seat=" + seat +
                ", price=" + price +
                ", date='" + date + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ticket ticket = (Ticket) o;

        if (isBooked != ticket.isBooked) return false;
        if (Double.compare(ticket.price, price) != 0) return false;
        if (event != null ? !event.equals(ticket.event) : ticket.event != null) return false;
        if (seat != null ? !seat.equals(ticket.seat) : ticket.seat != null) return false;
        return date != null ? date.equals(ticket.date) : ticket.date == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = event != null ? event.hashCode() : 0;
        result = 31 * result + (isBooked ? 1 : 0);
        result = 31 * result + (seat != null ? seat.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
