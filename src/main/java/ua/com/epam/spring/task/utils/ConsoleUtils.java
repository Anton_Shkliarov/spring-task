package ua.com.epam.spring.task.utils;


import java.util.Scanner;

public class ConsoleUtils {

    public static String readFromConsole() {
        Scanner scanner = new Scanner(System.in);
        String input = null;
        if (scanner.hasNextLine()){
            input = scanner.nextLine();
        }
        return input;
    }
}
