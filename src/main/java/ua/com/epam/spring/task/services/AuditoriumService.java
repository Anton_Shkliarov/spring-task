package ua.com.epam.spring.task.services;


import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.com.epam.spring.task.dao.AuditoriumsDAO;
import ua.com.epam.spring.task.entities.Auditorium;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AuditoriumService {

    @Autowired
    @Resource(name = "auditoriumsDao")
    private AuditoriumsDAO auditoriumsDAO;

    @Autowired
    @Resource(name = "auditoriumsList")
    private List<Auditorium> auditoriumList;

    public List<Auditorium> getAuditoriums() {
        return auditoriumsDAO.getAllAuditoriums();
    }

    public void init() {
        for (Auditorium auditorium : auditoriumList) {
            auditoriumsDAO.createAuditorium(auditorium.getName(), auditorium.getNumberOfSeats(), auditorium.getVipSeats());
        }
    }

}
