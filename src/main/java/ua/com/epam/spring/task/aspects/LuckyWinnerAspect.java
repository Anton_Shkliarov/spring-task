package ua.com.epam.spring.task.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import ua.com.epam.spring.task.entities.Ticket;
import ua.com.epam.spring.task.entities.User;

@Aspect
public class LuckyWinnerAspect {

    @Before("execution(public void bookTicket(..)) && args(user, ticket) && within(ua.com.epam.spring.task.services.BookingService))")
    private void allTicketBook(User user, Ticket ticket) {
        user.setLucky(false);
        String userName = user.getName();
        if (userName.contains("Q")) {
            ticket.setPrice(0);
            user.setLucky(true);
        }
    }
}
