package ua.com.epam.spring.task.utils;


public class EventsUtils implements UtilsInterface {

    public static void checkNameIsNotNull(String name) {
        if (EMPTY_STRING.equals(name)) {
            throw new IllegalArgumentException("You can't create an event with empty name");
        }
    }

    public static void checkPriceIsNotNull(int ticketPrice) {
        if (ticketPrice == 0){
            throw new IllegalArgumentException("Ticket price cannot be null");
        }
    }

    public static void checkRatingIsNotNull(String rating) {
        if (EMPTY_STRING.equals(rating))
            throw new IllegalArgumentException("Ticket price cannot be null");
    }
}
