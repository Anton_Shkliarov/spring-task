package ua.com.epam.spring.task.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import ua.com.epam.spring.task.entities.Event;
import ua.com.epam.spring.task.db.DBConstants;
import ua.com.epam.spring.task.db.QueryBuilder;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EventsDAO extends AbstractDAO {

    @Autowired
    @Resource(name = "queryBuilder")
    private QueryBuilder query;

    public void createEvent(String name, int ticketPrice, String rating, String auditorium) {
        query.clearQuery().insertInto(DBConstants.EVENTS_TABLE_NAME).values("?,?,?,?,?").endOfQuery();
        getJDBCTemplate().update(query.toString(), name, ticketPrice, rating, auditorium, false);
    }

    public void deleteEvent(String name) {
        query.clearQuery().deleteFrom(DBConstants.EVENTS_TABLE_NAME).where("name = ?").endOfQuery();
        getJDBCTemplate().update(query.toString(), name);
    }

    public Event getEventByName(String name) {
        query.clearQuery().select("*").from(DBConstants.EVENTS_TABLE_NAME).where("name = '" + name + "'");
        return getJDBCTemplate().queryForObject(query.toString(), getEventRowMapper());
    }

    public List<Event> getAllEvents() {
        query.clearQuery().select("*").from(DBConstants.EVENTS_TABLE_NAME);
        return getJDBCTemplate().query(query.toString(), getEventRowMapper());
    }

    private RowMapper<Event> getEventRowMapper() {
        return new RowMapper<Event>() {
            @Override
            public Event mapRow(ResultSet resultSet, int i) throws SQLException {
                String name = resultSet.getString("name");
                int ticketPrice = resultSet.getInt("ticket_price");
                String rating = resultSet.getString("rating");
                String  auditorium = resultSet.getString("auditorium");
                Boolean auditoriumIsAssigned = resultSet.getBoolean("auditorium_assigned");
                Event event = new Event(name, ticketPrice, rating, auditorium);
                event.setAuditoriumIsAssigned(auditoriumIsAssigned);
                return event;
            }
        };
    }
}
