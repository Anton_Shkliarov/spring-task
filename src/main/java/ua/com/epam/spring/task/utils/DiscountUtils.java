package ua.com.epam.spring.task.utils;


import ua.com.epam.spring.task.entities.Event;
import ua.com.epam.spring.task.entities.User;

import java.util.Date;

public class DiscountUtils {

    public static void checkUserIsNotNull(User user) {
        if (user == null)
            throw new IllegalArgumentException("We need User to calculate a discount");
    }

    public static void checkEventIsNotNull(Event event) {
        if (event == null)
            throw new IllegalArgumentException("We need Event to calculate a discount");
    }

    public static void checkFateIsNotNull(Date date) {
        if (date == null)
            throw new IllegalArgumentException("We need Date to calculate a discount");
    }
}
