package ua.com.epam.spring.task.entities;


public class Event {

    private String name;
    private int baseTicketPrice;
    private Rating rating;
    private String auditorium;

    private boolean auditoriumIsAssigned;

    public Event(String name, int baseTicketPrice, String rating, String auditorium) {
        this.name = name;
        this.baseTicketPrice = baseTicketPrice;
        this.rating = defineRating(rating);
        this.auditorium = auditorium;
    }

    public String getEventName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBaseTicketPrice() {
        return baseTicketPrice;
    }

    public void setBaseTicketPrice(int baseTicketPrice) {
        this.baseTicketPrice = baseTicketPrice;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public boolean isAuditoriumIsAssigned() {
        return auditoriumIsAssigned;
    }

    public void setAuditoriumIsAssigned(boolean auditoriumIsAssigned) {
        this.auditoriumIsAssigned = auditoriumIsAssigned;
    }

    public String getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(String auditorium) {
        this.auditorium = auditorium;
    }

    private Rating defineRating(String rating) {
        if (rating.equals("High"))
            return Rating.HIGH;
        if (rating.equals("Low"))
            return Rating.LOW;
        return Rating.MID;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", baseTicketPrice=" + baseTicketPrice +
                ", rating=" + rating +
                ", auditorium=" + auditorium +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (baseTicketPrice != event.baseTicketPrice) return false;
        if (auditoriumIsAssigned != event.auditoriumIsAssigned) return false;
        if (name != null ? !name.equals(event.name) : event.name != null) return false;
        if (rating != event.rating) return false;
        return auditorium != null ? auditorium.equals(event.auditorium) : event.auditorium == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + baseTicketPrice;
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (auditorium != null ? auditorium.hashCode() : 0);
        result = 31 * result + (auditoriumIsAssigned ? 1 : 0);
        return result;
    }
}
