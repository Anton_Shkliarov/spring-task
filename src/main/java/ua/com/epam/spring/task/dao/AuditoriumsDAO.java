package ua.com.epam.spring.task.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import ua.com.epam.spring.task.entities.Auditorium;
import ua.com.epam.spring.task.db.DBConstants;
import ua.com.epam.spring.task.db.QueryBuilder;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AuditoriumsDAO extends AbstractDAO {

    @Autowired
    @Resource(name = "queryBuilder")
    private QueryBuilder query;

    public void createAuditorium(String name, String numberOfSeats, String vipSeats) {
        query.clearQuery().insertInto(DBConstants.AUDITORIUMS_TABLE_NAME).values("?,?,?").endOfQuery();
        getJDBCTemplate().update(query.toString(), name, numberOfSeats, vipSeats);
    }

    public void deleteAuditorium(String name) {
        query.clearQuery().deleteFrom(DBConstants.AUDITORIUMS_TABLE_NAME).where("name = ?");
        getJDBCTemplate().update(query.toString(), name);
    }

    public Auditorium getAuditoriumByName(String auditoriumName) {
        query.clearQuery().select("*").from(DBConstants.AUDITORIUMS_TABLE_NAME).where("name = '" + auditoriumName + "'").endOfQuery();
        return getJDBCTemplate().queryForObject(query.toString(), getAuditoriumRowMapper());
    }

    public List<Auditorium> getAllAuditoriums() {
        query.clearQuery().select("*").from(DBConstants.AUDITORIUMS_TABLE_NAME).endOfQuery();
        return getJDBCTemplate().query(query.toString(), getAuditoriumRowMapper());
    }

    private RowMapper<Auditorium> getAuditoriumRowMapper() {
        return new RowMapper<Auditorium>() {
            @Override
            public Auditorium mapRow(ResultSet resultSet, int i) throws SQLException {
                String name = resultSet.getString("name");
                String seats = resultSet.getString("seats");
                String vips = resultSet.getString("vips");
                return new Auditorium(name, seats, vips);
            }
        };
    }






}
