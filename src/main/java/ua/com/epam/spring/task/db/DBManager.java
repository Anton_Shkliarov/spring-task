package ua.com.epam.spring.task.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;

public class DBManager {

    @Autowired
    @Resource(name = "jdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Resource(name = "queryBuilder")
    private QueryBuilder query;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void init() {
        createAuditoriumsTable();
        createEventsTable();
        createTicketsTable();
        createUsersTable();
    }

    private void createAuditoriumsTable() {
        query.clearQuery();
        query.createTableWithFields(DBConstants.AUDITORIUMS_TABLE_NAME, DBConstants.AUDITORIUMS_PARAMS);
        jdbcTemplate.execute(query.toString());
    }

    private void createEventsTable() {
        query.clearQuery();
        query.createTableWithFields(DBConstants.EVENTS_TABLE_NAME, DBConstants.EVENTS_PARAMS);
        jdbcTemplate.execute(query.toString());
    }

    private void createTicketsTable() {
        query.clearQuery();
        query.createTableWithFields(DBConstants.TICKETS_TABLE_NAME, DBConstants.TICKETS_PARAMS);
        jdbcTemplate.execute(query.toString());
    }

    private void createUsersTable() {
        query.clearQuery();
        query.createTableWithFields(DBConstants.USERS_TABLE_NAME, DBConstants.USERS_PARAMS);
        jdbcTemplate.execute(query.toString());
    }

}
