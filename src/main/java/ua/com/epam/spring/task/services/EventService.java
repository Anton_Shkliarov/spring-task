package ua.com.epam.spring.task.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.com.epam.spring.task.dao.EventsDAO;
import ua.com.epam.spring.task.entities.Auditorium;
import ua.com.epam.spring.task.entities.Event;
import ua.com.epam.spring.task.utils.EventsUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EventService {

    @Autowired
    @Resource(name = "eventsDao")
    private EventsDAO eventsDAO;

    public void create(String name, int ticketPrice, String rating, Auditorium auditorium) {
        EventsUtils.checkNameIsNotNull(name);
        EventsUtils.checkPriceIsNotNull(ticketPrice);
        EventsUtils.checkRatingIsNotNull(rating);
        Event event = new Event(name, ticketPrice, rating, auditorium.getName());
        if (!eventExistsInBase(event))
            eventsDAO.createEvent(name, ticketPrice, rating, auditorium.getName());
        else
            throw new IllegalArgumentException("Event " + event.toString() + " already exists in base");
    }

    public void remove(String name) {
        EventsUtils.checkNameIsNotNull(name);
        eventsDAO.deleteEvent(name);
    }

    public Event getByName(String eventName) {
        EventsUtils.checkNameIsNotNull(eventName);
        return eventsDAO.getEventByName(eventName);
    }

    public List<Event> getAll() {
        return eventsDAO.getAllEvents();
    }

    private boolean eventExistsInBase(Event event) {
        return eventsDAO.getAllEvents().contains(event);
    }
}
