package ua.com.epam.spring.task.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.com.epam.spring.task.dao.UsersDAO;
import ua.com.epam.spring.task.entities.Ticket;
import ua.com.epam.spring.task.entities.User;
import ua.com.epam.spring.task.utils.UserUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UsersDAO usersDAO;

    public void register(String name, String birthDate, String email) throws ParseException {
        UserUtils.checkNameIsNotNull(name);
        UserUtils.checkEmailIsNotNull(email);
        UserUtils.checkBirthDateIsNotNull(birthDate);
        User user = new User(name, birthDate, email);
        if (!usersDAO.getAllUsers().contains(user)) {
            usersDAO.createUser(name, birthDate, email);
        } else {
            throw new IllegalArgumentException("Sorry, that user already exists in the base. Please enter his data again.");
        }
    }

    public void remove(String email) {
        UserUtils.checkEmailIsNotNull(email);
        usersDAO.deleteUser(email);
    }

    public User getUserById(int id) {
        UserUtils.checkIdIsNotNull(id);
        return usersDAO.getUserById(id);
    }

    public User getUserByEmail(String email) {
        UserUtils.checkEmailIsNotNull(email);
        return usersDAO.getUserByEmail(email);
    }

    public List<User> getUsersByName(String name) {
        UserUtils.checkNameIsNotNull(name);
        return usersDAO.getUsersByName(name);
    }

    public boolean checkUserIsLucky(String userEmail) {
        User user = getUserByEmail(userEmail);
        return user.isLucky();
    }

    public void incrementUserBookedTickets(String userName) {
        usersDAO.updateUserBookedTickets(userName);
    }
}
