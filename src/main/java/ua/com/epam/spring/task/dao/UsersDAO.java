package ua.com.epam.spring.task.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import ua.com.epam.spring.task.entities.User;
import ua.com.epam.spring.task.db.DBConstants;
import ua.com.epam.spring.task.db.QueryBuilder;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UsersDAO extends AbstractDAO {

    @Autowired
    @Resource(name = "queryBuilder")
    private QueryBuilder query;

    public void createUser(String name, String birthDate, String email) {
        query.clearQuery().insertInto(DBConstants.USERS_TABLE_NAME).tableFields("name, birth_date, email").values("?,?,?");
        getJDBCTemplate().update(query.toString(), name, birthDate, email);
    }

    public void deleteUser(String email) {
        query.clearQuery().deleteFrom(DBConstants.USERS_TABLE_NAME).where("email = ?");
        getJDBCTemplate().update(query.toString(), email);
    }

    public User getUserById(int id) {
        query.clearQuery().select("*").from(DBConstants.USERS_TABLE_NAME).where("id = '" + id + "'");
        return getJDBCTemplate().queryForObject(query.toString(), getUserRowMapper());
    }

    public User getUserByEmail(String email) {
        query.clearQuery().select("*").from(DBConstants.USERS_TABLE_NAME).where("email = '" + email + "'");
        return getJDBCTemplate().queryForObject(query.toString(), getUserRowMapper());
    }

    public List<User> getUsersByName(String name) {
        query.clearQuery().select("*").from(DBConstants.USERS_TABLE_NAME).where("name = '" + name + "'");
        return getJDBCTemplate().query(query.toString(), getUserRowMapper());
    }

    public List<User> getAllUsers() {
        query.clearQuery().select("*").from(DBConstants.USERS_TABLE_NAME);
        return getJDBCTemplate().query(query.toString(), getUserRowMapper());
    }

    public void updateUserBookedTickets(String userName) {
        query.clearQuery().select("booked_tickets").from(DBConstants.USERS_TABLE_NAME).where("name = ?");
        int bookedTickets = getJDBCTemplate().queryForObject(query.toString(), Integer.class, userName);
        bookedTickets++;
        query.clearQuery().update(DBConstants.USERS_TABLE_NAME).set("booked_tickets = ?").where("name = ?");
        getJDBCTemplate().update(query.toString(), bookedTickets, userName);
    }

    private RowMapper<User> getUserRowMapper() {
        return new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                String birthDate = resultSet.getString("birth_date");
                Integer bookedTickets = resultSet.getInt("booked_tickets");
                Boolean isLucky = resultSet.getBoolean("is_lucky");
                User user = new User(name, birthDate, email);
                user.setLucky(isLucky);
                user.setBookedTickets(bookedTickets);
                return user;
            }
        };
    }

}
